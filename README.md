howCMS基础权限开发平台是基于thinkjs + vue+RBAC开发的前后端分离项目，是一个轻量级的快速开发后台管理系统架构，它只包含了 Element UI & axios & iconfont & permission control & lint，这些搭建后台必要的东西，您可以使用 howCMS快速进行二次开发，上手容易。
## 演示地址
[http://thinkjs.howeaver.cn/](http://thinkjs.howeaver.cn/)  
## 前端项目  
### 项目地址
[https://gitee.com/binger_2019/howcms-1.0-element-web.git](https://gitee.com/binger_2019/howcms-1.0-element-web.git)  
### 技术选型
* JS框架：vue
* UI框架：vue-element-admin
* 富文本：Quill
### 功能点
1. 系统基础管理
   - 1.1 用户管理 
   - 1.2 机构管理  
   - 1.3 用户管理
   - 1.4 角色管理
   - 1.5 菜单管理
   - 1.6 字典管理
   - 1.7 接口文档
   - 1.8 系统日志（通过请求接口地址和接口文档，自动生成系统日志）
   
2. 内容管理
   - 2.1 文章管理
   - 2.2 分类管理 
### Build Setup
```bash
#### 克隆项目
git clone https://gitee.com/binger_2019/howcms-1.0-element-web.git

#### 进入项目目录
cd vue-admin-template

#### 安装依赖
npm install

#### 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

#### 启动服务
npm run dev
```
浏览器访问 [http://localhost:9528](http://localhost:9528)
### 发布

```bash
#### 构建测试环境
npm run build:stage

#### 构建生产环境
npm run build:prod
```
### 账号密码
超级管理员帐号为admin 123456，后台所有帐号的默认密码都为123456

## 后端项目
### 项目地址
[https://gitee.com/binger_2019/howcms-1.0-thinkjs.git](https://gitee.com/binger_2019/howcms-1.0-thinkjs.git) 
### 技术选型
* 基础语言：Node.js
* 框架：koa2+ThinkJS
* 数据库：MySql
* 接口文档:apiDoc
* 接口安全:jwt
* 自动生成接口:RESTful
* 跨域处理:koa2-cors
### 项目服务启动  
* 安装依赖：npm install
* 启动程序：npm start
### 技术特点
1. 采用apiDoc做接口文档说明，写注释就能生成文档的工具；
2. 使用JSON Web Token做接口安全认证，包括跨域认证、单点登录、过期验证、token续签
3. 使用RESTful快速生成接口，thinkjs controller user，【由于已对自动生成的rest.js文件进行了重写，-r命令会把其覆盖，所以不要加-r命令，需在控制器手动继承rest.js文件】
4. 通过请求接口地址和接口文档，自动生成系统日志
### APIDOC 生成API
运行命令  
    apidoc -i src/ -o www/static/apidoc/
### 配置数据库
本项目使用mysql数据库，sql文件导入到本地数据库即可，配置文件的位置:src/config/adapter.js  
```javascript
 exports.model = {
        type: 'mysql',
        common: {
            logConnect: isDev,
            logSql: isDev,
            logger: msg => think.logger.info(msg)
        },
        mysql: {
            handle: mysql,
            database: 'thinkjs_demo',
            prefix: '',
            encoding: 'utf8',
            // host: '127.0.0.1',
            host: '49.233.27.66',
            port: '3306',
            user: 'thinkjs_demo',
            password: '**********',
            dateStrings: true
        }
    };
```
### 跨域配置
本系统采用前后端完全分离，采用koa-cors解决跨域的问题，Koa-cors 是基于 node-cors 开发的 Koa CORS中间件。
* 安装
```
$ npm install koa-cors --save
```
* 配置
配置文件的位置:src/config/middleware.js  
```javascript
{
    handle: cors, // 全局处理跨域，所有请求都会允许跨域，如果想要进行相关配置请看kcors文档进行配置
    options: {
      origin: (ctx) => { // 设置允许来自指定域名请求
        return isDev ? 'http://localhost:9528' : 'http://thinkjs.howeaver.cn'; // 只允许http://localhost:8080这个域名的请求
      },
      credentials: true, // 是否允许发送Cookie
      allowMethods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'], // 设置所允许的HTTP请求方法
      allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'jwt-token'], // 设置服务器支持的所有头信息字段
      exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'] // 设置获取其他自定义字段
    }
}
```
## 后台界面
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155317_2b24cbf8_4790316.png "登录.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155738_9fae2783_4790316.png "机构管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155931_9d101c5b_4790316.png "用户管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155859_43d63cf0_4790316.png "添加用户.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155753_4a049a52_4790316.png "角色管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155840_b5addc5a_4790316.png "添加角色.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155730_24939eda_4790316.png "菜单管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155830_5aa1fe83_4790316.png "添加菜单.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155820_ccd2bb1c_4790316.png "栏目管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155940_8de9a0ad_4790316.png "字典管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155805_d22d749f_4790316.png "接口文档.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155922_e3120ff6_4790316.png "系统日志.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155907_f54ceb2f_4790316.png "文章管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/155850_6e3d1594_4790316.png "添加文章.png")





