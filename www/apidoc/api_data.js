define({ "api": [
  {
    "type": "delete",
    "url": "/user/delete/",
    "title": "删除用户",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>用户id（必填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"id\": \"5\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": " {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"affectedRows\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/user.js",
    "groupTitle": "User",
    "name": "DeleteUserDelete",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/user/delete/"
      }
    ]
  },
  {
    "type": "get",
    "url": "/user/checkPass/",
    "title": "验证原始密码是否正确",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>用户Id（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码（必填）</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/user.js",
    "groupTitle": "User",
    "name": "GetUserCheckpass",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/user/checkPass/"
      }
    ]
  },
  {
    "type": "get",
    "url": "/user/checkPass/",
    "title": "验证原始密码是否正确",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>用户Id（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码（必填）</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/user.js",
    "groupTitle": "User",
    "name": "GetUserCheckpass",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/user/checkPass/"
      }
    ]
  },
  {
    "type": "Get",
    "url": "/user/get/",
    "title": "获取用户列表",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>用户id(选填)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n id:1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n  \"errno\": 0,\n  \"errmsg\": \"\",\n  \"data\": [\n      {\n          \"id\": 1,\n          \"code\": null,\n          \"name\": \"admin\",\n          \"password\": \"e10adc3949ba59abbe56e057f20f883e\"\n      }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/user.js",
    "groupTitle": "User",
    "name": "GetUserGet",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/user/get/"
      }
    ]
  },
  {
    "type": "get",
    "url": "/user/user/",
    "title": "获取登录用户的id",
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"id\": 1,\n        \"iat\": 1616983497,\n        \"exp\": 1617069897\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/user.js",
    "groupTitle": "User",
    "name": "GetUserUser",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/user/user/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/login/",
    "title": "用户登录",
    "group": "auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>用户名(必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码(必填)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"name\":\"admin\",\n \"password\": \"e10adc3949ba59abbe56e057f20f883e\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n  \"errno\": 0,\n  \"errmsg\": \"\",\n  \"data\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiYWRtaW4iLCJpYXQiOjE2MTY2MzEwOTAsImV4cCI6MTYxNjcxNzQ5MH0.Ahsy5Qwig7jcCpJyWk4Gu8BBXq5IKM3-toJvLsSz138\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/auth.js",
    "groupTitle": "auth",
    "name": "PostAuthLogin",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/auth/login/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/logout/",
    "title": "用户退出登录",
    "group": "auth",
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n   \"errno\": 0,\n   \"errmsg\": \"\",\n   \"data\": \"退出登录成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/auth.js",
    "groupTitle": "auth",
    "name": "PostAuthLogout",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/auth/logout/"
      }
    ]
  },
  {
    "type": "delete",
    "url": "/department/delete/",
    "title": "删除单位",
    "group": "department",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>单位id（必填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"id\": \"5\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": " {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"affectedRows\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/department.js",
    "groupTitle": "department",
    "name": "DeleteDepartmentDelete",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/department/delete/"
      }
    ]
  },
  {
    "type": "Get",
    "url": "/department/get/",
    "title": "获取单位列表",
    "group": "department",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>单位id(选填)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n id:1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"id\": 72,\n        \"name\": \"蓝天小区\",\n        \"pid\": 0,\n        \"mpSetting\": null,\n        \"dashboardSetting\": null,\n        \"orderNum\": null,\n        \"create_date\": \"1591669444\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/department.js",
    "groupTitle": "department",
    "name": "GetDepartmentGet",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/department/get/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/department/getAuth/",
    "title": "获取本级及所有子级节点",
    "group": "department",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>单位根id（必填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"pid\": \"73\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": " {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": [\n        {\n            \"id\": 91\n        },\n        {\n            \"id\": 92\n        },\n        {\n            \"id\": 101\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/department.js",
    "groupTitle": "department",
    "name": "PostDepartmentGetauth",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/department/getAuth/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/department/getPath/",
    "title": "生成具有路径的单位列表",
    "group": "department",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>单位根id（必填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"pid\": \"94\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "        {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": [\n        {\n            \"id\": 94,\n            \"name\": \"二连\",\n            \"pid\": 91,\n            \"mp_setting\": null,\n            \"dashboard_setting\": null,\n            \"orderNum\": 0,\n            \"create_date\": \"1596162380\",\n            \"path\": \"二连\"\n        },\n        {\n            \"id\": 99,\n            \"name\": \"一班\",\n            \"pid\": 94,\n            \"mp_setting\": null,\n            \"dashboard_setting\": null,\n            \"orderNum\": 0,\n            \"create_date\": \"1596162418\",\n            \"path\": \"二连/一班\"\n        },\n        {\n            \"id\": 100,\n            \"name\": \"二班\",\n            \"pid\": 94,\n            \"mp_setting\": null,\n            \"dashboard_setting\": null,\n            \"orderNum\": 0,\n            \"create_date\": \"1596162423\",\n            \"path\": \"二连/二班\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/department.js",
    "groupTitle": "department",
    "name": "PostDepartmentGetpath",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/department/getPath/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/department/getTree/",
    "title": "生成单位树状列表",
    "group": "department",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>单位根id（必填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"pid\": \"94\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": " {\n\"errno\": 0,\n\"errmsg\": \"\",\n\"data\": [\n            {\n                \"id\": 99,\n                \"name\": \"一班\",\n                \"pid\": 94,\n                \"mp_setting\": null,\n                \"dashboard_setting\": null,\n                \"orderNum\": 0,\n                \"create_date\": \"1596162418\",\n                \"path\": \"一班\"\n            },\n            {\n                \"id\": 100,\n                \"name\": \"二班\",\n                \"pid\": 94,\n                \"mp_setting\": null,\n                \"dashboard_setting\": null,\n                \"orderNum\": 0,\n                \"create_date\": \"1596162423\",\n                \"path\": \"二班\"\n            }\n        ]\n    }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/department.js",
    "groupTitle": "department",
    "name": "PostDepartmentGettree",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/department/getTree/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/department/post/",
    "title": "增加单位",
    "group": "department",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>单位名（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>父id（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mp_setting",
            "description": "<p>小程序首页设置参数(json格式)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dashboard_setting",
            "description": "<p>首页大屏幕设置参数(json格式)</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "orderNum",
            "description": "<p>排序（越大越靠前）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n        \"name\": \"蓝天小区\",\n        \"pid\": 0,\n        \"mpSetting\": null,\n        \"dashboardSetting\": null,\n        \"orderNum\": null,\n        \"create_date\": \"1591669444\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"id\": 105\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/department.js",
    "groupTitle": "department",
    "name": "PostDepartmentPost",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/department/post/"
      }
    ]
  },
  {
    "type": "put",
    "url": "/department/put/",
    "title": "编辑单位",
    "group": "department",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>单位Id（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>单位名</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>父id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mp_setting",
            "description": "<p>小程序首页设置参数(json格式)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dashboard_setting",
            "description": "<p>首页大屏幕设置参数(json格式)</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "orderNum",
            "description": "<p>排序（越大越靠前）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n       \"name\": \"蓝天小区\",\n       \"pid\": 0,\n       \"mpSetting\": null,\n       \"dashboardSetting\": null,\n       \"orderNum\": null,\n       \"create_date\": \"1591669444\",\n       \"id\":1\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"affectedRows\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/department.js",
    "groupTitle": "department",
    "name": "PutDepartmentPut",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/department/put/"
      }
    ]
  },
  {
    "type": "delete",
    "url": "/privilege/delete/",
    "title": "删除权限",
    "group": "privilege",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>权限id（必填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"id\": \"5\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": " {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"affectedRows\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/privilege.js",
    "groupTitle": "privilege",
    "name": "DeletePrivilegeDelete",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/privilege/delete/"
      }
    ]
  },
  {
    "type": "Get",
    "url": "/privilege/get/",
    "title": "获取权限列表",
    "group": "privilege",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>权限id(选填)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n id:1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"id\": 16,\n        \"title\": \"系统管理\",\n        \"name\": \"/system\",\n        \"type\": 1,\n        \"orderNum\": 0,\n        \"percode\": \"\",\n        \"icon\": \"\",\n        \"pid\": 0\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/privilege.js",
    "groupTitle": "privilege",
    "name": "GetPrivilegeGet",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/privilege/get/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/privilege/getTree/",
    "title": "生成分类树状列表",
    "group": "privilege",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>分类根id（必填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"pid\": \"94\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "     {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": [\n\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/category.js",
    "groupTitle": "privilege",
    "name": "PostPrivilegeGettree",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/privilege/getTree/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/privilege/getTree/",
    "title": "生成权限树状列表",
    "group": "privilege",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>权限根id（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "type",
            "description": "<p>类型,1是菜单，2是按钮（选填，如果为空显示全部）</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "role",
            "description": "<p>所属角色</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"pid\": \"94\",\n \"role\":[12,13],\n \"type\":1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "     {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": [\n        {\n            \"id\": 16,\n            \"title\": \"系统管理\",\n            \"name\": \"/system\",\n            \"type\": 1,\n            \"orderNum\": 0,\n            \"percode\": \"\",\n            \"icon\": \"\",\n            \"pid\": 0,\n            \"path\": \"系统管理\",\n            \"children\": [\n                {\n                    \"id\": 17,\n                    \"title\": \"用户管理\",\n                    \"name\": \"/system/user\",\n                    \"type\": 1,\n                    \"orderNum\": null,\n                    \"percode\": \"\",\n                    \"icon\": \"\",\n                    \"pid\": 16,\n                    \"path\": \"系统管理/用户管理\"\n                }\n            ]\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/privilege.js",
    "groupTitle": "privilege",
    "name": "PostPrivilegeGettree",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/privilege/getTree/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/privilege/post/",
    "title": "增加权限",
    "group": "privilege",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>权限名（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>父id（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>权限的地址</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "type",
            "description": "<p>类型,1是菜单，2是按钮</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "percode",
            "description": "<p>按钮权限的标识</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "orderNum",
            "description": "<p>排序（越大越靠前）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>权限的图标</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n        \"title\": \"系统管理\",\n        \"name\": \"/system\",\n        \"type\": 1,\n        \"orderNum\": 0,\n        \"percode\": '',\n        \"icon\": \"\",\n        \"pid\":0\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"id\": 105\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/privilege.js",
    "groupTitle": "privilege",
    "name": "PostPrivilegePost",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/privilege/post/"
      }
    ]
  },
  {
    "type": "put",
    "url": "/privilege/put/",
    "title": "编辑权限",
    "group": "privilege",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>权限Id（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>权限名</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>父id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>权限的地址</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "type",
            "description": "<p>类型,1是菜单，2是按钮</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "percode",
            "description": "<p>按钮权限的标识</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "orderNum",
            "description": "<p>排序（越大越靠前）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>权限的图标</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n    \"id\": 1,\n       \"title\": \"系统管理\",\n       \"name\": \"/system\",\n       \"type\": 1,\n       \"orderNum\": 0,\n       \"percode\": '',\n       \"icon\": \"\",\n       \"pid\":0\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"affectedRows\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/privilege.js",
    "groupTitle": "privilege",
    "name": "PutPrivilegePut",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/privilege/put/"
      }
    ]
  },
  {
    "type": "delete",
    "url": "/role/delete/",
    "title": "删除角色",
    "group": "role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>角色id（必填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"id\": \"5\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": " {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"affectedRows\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/role.js",
    "groupTitle": "role",
    "name": "DeleteRoleDelete",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/role/delete/"
      }
    ]
  },
  {
    "type": "Get",
    "url": "/role/get/",
    "title": "获取角色列表",
    "group": "role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>角色id(选填)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n id:1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n  \"errno\": 0,\n  \"errmsg\": \"\",\n  \"data\": [\n      {\n          \"id\": 1,\n          \"name\": \"admin\"\n      }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/role.js",
    "groupTitle": "role",
    "name": "GetRoleGet",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/role/get/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/role/post/",
    "title": "增加角色",
    "group": "role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>角色名（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "menuIds",
            "description": "<p>角色-权限</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "remark",
            "description": "<p>角色备注</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n menuIds: [38, 40, 27],\n          name: \"4台人员\",\n          remark: \"微软天后宫\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n   \"errno\": 0,\n   \"errmsg\": \"\",\n   \"data\": {\n       \"id\": 5\n   }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/role.js",
    "groupTitle": "role",
    "name": "PostRolePost",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/role/post/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/role/post/",
    "title": "增加用户",
    "group": "role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>用户名（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "menuIds",
            "description": "<p>用户-权限</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "remark",
            "description": "<p>用户备注</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n menuIds: [38, 40, 27],\n          name: \"4台人员\",\n          remark: \"微软天后宫\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n   \"errno\": 0,\n   \"errmsg\": \"\",\n   \"data\": {\n       \"id\": 5\n   }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/user.js",
    "groupTitle": "role",
    "name": "PostRolePost",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/role/post/"
      }
    ]
  },
  {
    "type": "put",
    "url": "/role/put/",
    "title": "编辑角色",
    "group": "role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>角色Id（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>角色名（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "menuIds",
            "description": "<p>角色-权限</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "remark",
            "description": "<p>角色备注</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n          id: 26\n          menuIds: [33, 27]\n          name: \"test\"\n          remark: \"备注\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\":''\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/role.js",
    "groupTitle": "role",
    "name": "PutRolePut",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/role/put/"
      }
    ]
  },
  {
    "type": "put",
    "url": "/role/put/",
    "title": "编辑用户",
    "group": "role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>用户Id（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>用户名（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "menuIds",
            "description": "<p>用户-权限</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "remark",
            "description": "<p>用户备注</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n              id: 26\n              menuIds: [33, 27]\n              name: \"test\"\n              remark: \"备注\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\":''\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/user.js",
    "groupTitle": "role",
    "name": "PutRolePut",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/role/put/"
      }
    ]
  },
  {
    "type": "delete",
    "url": "/role_privilege/delete/",
    "title": "删除角色_权限",
    "group": "role_privilege",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>权限编号（选填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "rid",
            "description": "<p>角色编号（选填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "create_by",
            "description": "<p>创建人（选填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"pid\":1,\n    \"rid\":5,\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": " {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"affectedRows\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/role_privilege.js",
    "groupTitle": "role_privilege",
    "name": "DeleteRole_privilegeDelete",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/role_privilege/delete/"
      }
    ]
  },
  {
    "type": "Get",
    "url": "/role_privilege/get/",
    "title": "获取角色_权限列表",
    "group": "role_privilege",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>权限编号（选填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "rid",
            "description": "<p>角色编号（选填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "create_by",
            "description": "<p>创建人（选填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"pid\":1,\n    \"rid\":5,\n    \"create_by\":1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "  {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": [\n        {\n            \"pid\": 1,\n            \"rid\": 4,\n            \"create_by\": 1,\n            \"create_date\": \"1591669444\"\n        },\n        {\n            \"pid\": 1,\n            \"rid\": 5,\n            \"create_by\": 1,\n            \"create_date\": \"1591669444\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/role_privilege.js",
    "groupTitle": "role_privilege",
    "name": "GetRole_privilegeGet",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/role_privilege/get/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/role_privilege/post/",
    "title": "增加角色_权限",
    "group": "role_privilege",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "pid",
            "description": "<p>权限编号（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "rid",
            "description": "<p>角色编号（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "create_by",
            "description": "<p>创建人</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create_date",
            "description": "<p>创建时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"pid\":1\n    \"rid\":5\n    \"create_by\":1\n    \"create_date\":1591669444\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n   \"errno\": 0,\n   \"errmsg\": \"\",\n   \"data\": {\n       \"id\": 5\n   }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/role_privilege.js",
    "groupTitle": "role_privilege",
    "name": "PostRole_privilegePost",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/role_privilege/post/"
      }
    ]
  },
  {
    "type": "delete",
    "url": "/user_role/delete/",
    "title": "删除用户_角色",
    "group": "user_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "uid",
            "description": "<p>用户编号（选填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "rid",
            "description": "<p>角色编号（选填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "create_by",
            "description": "<p>创建人（选填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"uid\":1,\n    \"rid\":5,\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": " {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": {\n        \"affectedRows\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/user_role.js",
    "groupTitle": "user_role",
    "name": "DeleteUser_roleDelete",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/user_role/delete/"
      }
    ]
  },
  {
    "type": "Get",
    "url": "/user_role/get/",
    "title": "获取用户_角色列表",
    "group": "user_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "uid",
            "description": "<p>用户编号（选填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "rid",
            "description": "<p>角色编号（选填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "create_by",
            "description": "<p>创建人（选填）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"uid\":1,\n    \"rid\":5,\n    \"create_by\":1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "  {\n    \"errno\": 0,\n    \"errmsg\": \"\",\n    \"data\": [\n        {\n            \"uid\": 1,\n            \"rid\": 4,\n            \"create_by\": 1,\n            \"create_date\": \"1591669444\"\n        },\n        {\n            \"uid\": 1,\n            \"rid\": 5,\n            \"create_by\": 1,\n            \"create_date\": \"1591669444\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/user_role.js",
    "groupTitle": "user_role",
    "name": "GetUser_roleGet",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/user_role/get/"
      }
    ]
  },
  {
    "type": "post",
    "url": "/user_role/post/",
    "title": "增加用户_角色",
    "group": "user_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "uid",
            "description": "<p>用户编号（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "rid",
            "description": "<p>角色编号（必填）</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "create_by",
            "description": "<p>创建人</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create_date",
            "description": "<p>创建时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求示例",
          "content": "{\n \"uid\":1\n    \"rid\":5\n    \"create_by\":1\n    \"create_date\":1591669444\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "返回示例",
          "content": "{\n   \"errno\": 0,\n   \"errmsg\": \"\",\n   \"data\": {\n       \"id\": 5\n   }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/user_role.js",
    "groupTitle": "user_role",
    "name": "PostUser_rolePost",
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8360/user_role/post/"
      }
    ]
  }
] });
