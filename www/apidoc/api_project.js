define({
  "name": "thinkjs-demo",
  "version": "1.0.0",
  "description": "通过thinkjs逆向研究园区平台",
  "title": "学习thinkjs",
  "url": "",
  "sampleUrl": "http://127.0.0.1:8360",
  "header": {
    "title": "文档说明",
    "content": "<h2>写在开头</h2>\n<p>在本项目的研发过程中，得到了许多来做各大社区朋友的帮忙，非常感谢</p>\n<h4>更新文档命令：apidoc -i src/ -o www/apidoc/</h4>\n"
  },
  "footer": {
    "title": "文档结尾",
    "content": "<h2>谢谢</h2>\n<p>以上为文档的全部内容，如有疑问请联系负责人</p>\n"
  },
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-10-08T01:26:25.286Z",
    "url": "https://apidocjs.com",
    "version": "0.27.1"
  }
});
