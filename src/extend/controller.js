const jsonwebtoken = require('jsonwebtoken');
// const { ctx } = require('../controller/base');
module.exports = {
  authFail() {
    return this.fail('JWT 验证失败');
  },
  async checkAuth() {
    const { secret, cookie } = this.config('jwt');
    const token = await this.ctx.headers[cookie];
    try {
      // 解密token
      var tokenObj = token ? jsonwebtoken.verify(token, secret) : {};
      this.ctx.state.userId = tokenObj.userId;
      const savedToken = await this.cache('cache' + tokenObj.userId);
      var savedTokenObj = jsonwebtoken.verify(savedToken, secret);
      // console.log(5555, tokenObj);
      // 判断是否在其他终端登录
      if (savedTokenObj.timeSign !== tokenObj.timeSign) {
        return this.fail(10001, '用户在其他终端登录');
      }
    } catch (error) {
      return this.authFail();
    }

    if (!tokenObj.userId) {
      return this.authFail();
    }
    // 续签
    this.updateAuth({
      userId: tokenObj.userId,
      name: tokenObj.name,
      timeSign: tokenObj.timeSign
    });
  },
  // 更新token
  async updateAuth(obj) {
    const { secret, expire } = this.config('jwt');
    // 加密token并存到cookie和cache
    const token = jsonwebtoken.sign(obj, secret, { expiresIn: expire });
    // 需要将token返回给前端，用来续签
    this.ctx.set('Access-Control-Expose-Headers', 'jwt-token');
    this.ctx.set('jwt-token', token);
    // console.log(444, token);
    if (obj.userId) {
      await this.cache('cache' + obj.userId, token);
    } else {
      await this.cache('cache' + obj.userId, null);
    }
    return token;
  },

  // 将列表递归成树状结构
  recursionDataTree(dataList, pid, path = '', depth = 0) {
    depth++;
    const resultList = [];
    if (!dataList) return null;
    dataList.forEach(ele => {
      const bmidNew = ele['id'];
      const parentId = ele['pid'];
      if (pid == parentId) {
        ele['path'] = path ? path + '/' + ele['name'] : ele['name'];
        ele.depth = depth;
        const childrenList = this.recursionDataTree(dataList, bmidNew, ele['path'], depth);
        if (childrenList) { ele['children'] = childrenList }
        resultList.push(ele);
      }
    });

    if (resultList.length > 0) {
      return resultList;
    }
  },

  // 将列表递归成具有路径的树状结构
  recursionDataPath(dataList, pid) {
    const arr = [];
    let rootPath = '';
    dataList.forEach(ele => {
      if (pid == ele['id']) {
        ele['path'] = ele['name'];
        arr.push(ele);
        rootPath = ele['name'];
      }
    });
    const recursion = (dataList, pid, path = '') => {
      dataList.forEach(ele => {
        const bmidNew = ele['id'];
        const parentId = ele['pid'];
        if (pid == parentId) {
          ele['path'] = path ? path + '/' + ele['name'] : ele['name'];
          recursion(dataList, bmidNew, ele['path']);
          arr.push(ele);
        }
      });
    };
    recursion(dataList, pid, rootPath);
    return arr;
  },
  /**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string | null}
 */
  parseTime(time, cFormat) {
    if (arguments.length === 0 || !time) {
      return null;
    }
    const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}';
    let date;
    if (typeof time === 'object') {
      date = time;
    } else {
      if ((typeof time === 'string')) {
        if ((/^[0-9]+$/.test(time))) {
        // support "1548221490638"
          time = parseInt(time);
        } else {
        // support safari
        // https://stackoverflow.com/questions/4310953/invalid-date-in-safari
          time = time.replace(new RegExp(/-/gm), '/');
        }
      }

      if ((typeof time === 'number') && (time.toString().length === 10)) {
        time = time * 1000;
      }
      date = new Date(time);
    }
    const formatObj = {
      y: date.getFullYear(),
      m: date.getMonth() + 1,
      d: date.getDate(),
      h: date.getHours(),
      i: date.getMinutes(),
      s: date.getSeconds(),
      a: date.getDay()
    };
    const timeStr = format.replace(/{([ymdhisa])+}/g, (result, key) => {
      const value = formatObj[key];
      // Note: getDay() returns 0 on Sunday
      if (key === 'a') { return [ '日', '一', '二', '三', '四', '五', '六' ][ value ] }
      return value.toString().padStart(2, '0');
    });
    return timeStr;
  }
};
