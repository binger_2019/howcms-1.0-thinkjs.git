module.exports = class extends think.Model {
  login(query) {
    return this.where({name: query.name, password: query.password}).find();
  }
  get relation() {
    return {
      // department: {
      //   type: think.Model.BELONG_TO,
      //   key: 'did'
      // },
      role: {
        type: think.Model.MANY_TO_MANY,
        rModel: 'user_role',
        rfKey: 'rid',
        fKey: 'uid'
      }
    };
  }
  get schema() {
    return {
      id: { // 字段名称
        type: 'varchar(10)',
        default: 'small'
      }
    };
  }
};
