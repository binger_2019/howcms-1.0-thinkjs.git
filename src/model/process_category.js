module.exports = class extends think.Model {
  get relation() {
    return {
      process_definition: {
        relation: false,
        type: think.Model.HAS_MANY,
        fKey: 'processCategoryKey',
        key: 'processCategoryKey',
        order: 'id ASC'
      },
      task_name: {
        relation: false,
        type: think.Model.HAS_MANY,
        fKey: 'processCategoryKey',
        key: 'processCategoryKey',
        order: 'id ASC'
      }
    };
  }
};
