module.exports = class extends think.Model {
  get pk() {
    return 'id';
  }
  get relation() {
    return {
      process_instance: {
        type: think.Model.BELONG_TO,
        key: 'businessKey',
        fKey: 'businessKey'
      },
      task_instance: {
        relation: false,
        type: think.Model.HAS_MANY,
        key: 'businessKey',
        fKey: 'businessKey'
      },
      user: {
        relation: false,
        type: think.Model.BELONG_TO,
        key: 'startUserId',
        fKey: 'id',
        field: 'id,name'
      }

    };
  }
};
