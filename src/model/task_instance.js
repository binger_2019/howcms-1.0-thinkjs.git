module.exports = class extends think.Model {
  get pk() {
    return 'taskInstanceId';
  }
  get relation() {
    return {
      process_definition: {
        relation: false,
        type: think.Model.BELONG_TO,
        key: 'processDefinitionId',
        fKey: 'id'
      },
      process_instance: {
        relation: false,
        type: think.Model.BELONG_TO,
        key: 'processInstanceId',
        fKey: 'processInstanceId'
      },
      task_definition: {
        relation: false,
        type: think.Model.BELONG_TO,
        key: 'taskDefinitionId',
        fKey: 'taskDefinitionId'
      },
      user: {
        relation: false,
        type: think.Model.BELONG_TO,
        key: 'assignee',
        fKey: 'id',
        field: 'id,name'
      }

    };
  }
};
