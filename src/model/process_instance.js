module.exports = class extends think.Model {
  get pk() {
    return 'processInstanceId';
  }
  get relation() {
    return {
      user: {
        relation: false,
        type: think.Model.BELONG_TO,
        key: 'startUserId',
        fKey: 'id'
      },
      process_definition: {
        relation: false,
        type: think.Model.BELONG_TO,
        key: 'processDefinitionId',
        fKey: 'id'
      },
      task_instance: {
        relation: false,
        type: think.Model.HAS_MANY,
        key: 'processInstanceId',
        fKey: 'processInstanceId'
      }
    };
  }
};
