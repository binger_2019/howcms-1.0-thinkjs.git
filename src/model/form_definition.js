module.exports = class extends think.Model {
  get relation() {
    return {
      form_definition_fields: {
        relation: false,
        type: think.Model.HAS_MANY,
        key: 'id',
        fKey: 'form_definition_id',
        order: 'sort ASC'
      }
    };
  }
};
