module.exports = class extends think.Model {
  get relation() {
    return {
      category: {
        type: think.Model.BELONG_TO,
        key: 'cid'
      },
      department: {
        type: think.Model.BELONG_TO,
        key: 'did'
      }
    };
  }
};
