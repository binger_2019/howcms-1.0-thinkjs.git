module.exports = class extends think.Model {
  get relation() {
    return {
      dict_item: {
        type: think.Model.HAS_MANY,
        fKey: 'dict_id',
        key: 'id',
        order: 'sort_order DESC,id ASC'
      }
    };
  }
};
