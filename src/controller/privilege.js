/**
 * @api {Get} /privilege/get/ 获取权限列表
 * @apiGroup privilege
 *
 * @apiParam {Int} id 权限id(选填)
 * @apiParamExample {json} 请求示例
 * {
 *  id:1
 * }
 *
 * @apiSuccessExample  {json} 返回示例
{
    "errno": 0,
    "errmsg": "",
    "data": {
        "id": 16,
        "title": "系统管理",
        "name": "/system",
        "type": 1,
        "orderNum": 0,
        "percode": "",
        "icon": "",
        "pid": 0
    }
}
*/

/**
 * @api {post} /privilege/post/ 增加权限
 * @apiGroup privilege
 *
 * @apiParam {String} title 权限名（必填）
 * @apiParam {Int} pid 父id（必填）
 * @apiParam {String} name 权限的地址
 * @apiParam {Int} type 类型,1是菜单，2是按钮
 * @apiParam {Int} percode 按钮权限的标识
 * @apiParam {Int} orderNum 排序（越大越靠前）
 * @apiParam {String} icon 权限的图标
 * @apiParamExample {json} 请求示例
 * {
        "title": "系统管理",
        "name": "/system",
        "type": 1,
        "orderNum": 0,
        "percode": '',
        "icon": "",
        "pid":0
 * }
 *
 * @apiSuccessExample  {json} 返回示例
{
    "errno": 0,
    "errmsg": "",
    "data": {
        "id": 105
    }
}
 */

/**
 * @api {put} /privilege/put/ 编辑权限
 * @apiGroup privilege
 *
 * @apiParam {Int} id 权限Id（必填）
 * @apiParam {String} title 权限名
 * @apiParam {Int} pid 父id
 * @apiParam {String} name 权限的地址
 * @apiParam {Int} type 类型,1是菜单，2是按钮
 * @apiParam {Int} percode 按钮权限的标识
 * @apiParam {Int} orderNum 排序（越大越靠前）
 * @apiParam {String} icon 权限的图标
 * @apiParamExample {json} 请求示例
 *  {
 *      "id": 1,
        "title": "系统管理",
        "name": "/system",
        "type": 1,
        "orderNum": 0,
        "percode": '',
        "icon": "",
        "pid":0
  }
 *
 * @apiSuccessExample  {json} 返回示例
{
    "errno": 0,
    "errmsg": "",
    "data": {
        "affectedRows": 1
    }
}
 */

const BaseRest = require('./rest.js');

module.exports = class extends BaseRest {
  __before() {

  }

  /**
     * @api {post} /privilege/getTree/ 生成权限树状列表
     * @apiGroup privilege
     *
     * @apiParam {Int} pid 权限根id（必填）
     * @apiParam {Int} type 类型,1是菜单，2是按钮（选填，如果为空显示全部）
     * @apiParam {Array} role 所属角色
     * @apiParamExample {json} 请求示例
     * {
     *  "pid": "94",
     *  "role":[12,13],
     *  "type":1
     * }
     *
     * @apiSuccessExample  {json} 返回示例
     {
    "errno": 0,
    "errmsg": "",
    "data": [
        {
            "id": 16,
            "title": "系统管理",
            "name": "/system",
            "type": 1,
            "orderNum": 0,
            "percode": "",
            "icon": "",
            "pid": 0,
            "path": "系统管理",
            "children": [
                {
                    "id": 17,
                    "title": "用户管理",
                    "name": "/system/user",
                    "type": 1,
                    "orderNum": null,
                    "percode": "",
                    "icon": "",
                    "pid": 16,
                    "path": "系统管理/用户管理"
                }
            ]
        }
    ]
}
    */
  async getTreeAction() {
    const where = {};
    const param = this.post();
    if (param.type) {
      where.type = param.type;
    }
    if (param.rid && param.rid.length > 0) {
      const Arr = await this.model('role_privilege').where({ rid: ['IN', param.rid] }).getField('pid');
      if (Arr.length > 0) {
        where.id = ['IN', Arr];
      }
    }
    const dataList = await this.model('privilege').where(where).order('orderNum DESC').select();
    const res = this.recursionDataTree(dataList, param.pid);
    return this.success(res);
  }

  /**
 * @api {delete} /privilege/delete/ 删除权限
 * @apiGroup privilege
 *
 * @apiParam {Int} id 权限id（必填）
 * @apiParamExample {json} 请求示例
 * {
 *  "id": "5",
 * }
 *
 * @apiSuccessExample  {json} 返回示例
 {
    "errno": 0,
    "errmsg": "",
    "data": {
        "affectedRows": 1
    }
}
*/
  async deleteAction() {
    const privilegeModel = this.model('privilege');
    try {
      await privilegeModel.startTrans();
      // 删除角色
      const result = await privilegeModel.where({ id: this.post('id') }).delete();
      // 删除角色权限
      const rolePrivilegeModel = this.model('role_privilege').db(privilegeModel.db());
      await rolePrivilegeModel.where({ pid: this.post('id') }).delete();
      await privilegeModel.commit();
      return this.success(result);
    } catch (e) {
      await privilegeModel.rollback();
    }
  }
  // async testAction() {
  //   const Arr = await this.model('role_privilege').field('pid').where({rid: ['IN', param.rid]});
  //   return this.success(Arr);
  // }
};
