
/**
 * @api {post} /role_privilege/post/ 增加角色_权限
 * @apiGroup role_privilege
 *
 * @apiParam {Int} pid 权限编号（必填）
 * @apiParam {Int} rid 角色编号（必填）
 * @apiParam {Int} create_by 创建人
 * @apiParam {String} create_date 创建时间
 * @apiParamExample {json} 请求示例
 * {
 *  "pid":1
    "rid":5
    "create_by":1
    "create_date":1591669444
 * }
 *
 * @apiSuccessExample  {json} 返回示例
 {
    "errno": 0,
    "errmsg": "",
    "data": {
        "id": 5
    }
 }
 */

const BaseRest = require('./rest.js');

module.exports = class extends BaseRest {
  __before() {

  }

  /**
 * @api {Get} /role_privilege/get/ 获取角色_权限列表
 * @apiGroup role_privilege
 *
 * @apiParam {Int} pid 权限编号（选填）
 * @apiParam {Int} rid 角色编号（选填）
 * @apiParam {Int} create_by 创建人（选填）
 * @apiParamExample {json} 请求示例
 * {
 *  "pid":1,
    "rid":5,
    "create_by":1
 * }
 *
 * @apiSuccessExample  {json} 返回示例
  {
    "errno": 0,
    "errmsg": "",
    "data": [
        {
            "pid": 1,
            "rid": 4,
            "create_by": 1,
            "create_date": "1591669444"
        },
        {
            "pid": 1,
            "rid": 5,
            "create_by": 1,
            "create_date": "1591669444"
        }
    ]
}
*/
  // async getAction() {
  //   const dataList = await this.model('role_privilege').where(this.post()).select();
  //   return this.success(dataList);
  // }

  /**
 * @api {delete} /role_privilege/delete/ 删除角色_权限
 * @apiGroup role_privilege
 *
 * @apiParam {Int} pid 权限编号（选填）
 * @apiParam {Int} rid 角色编号（选填）
 * @apiParam {Int} create_by 创建人（选填）
 * @apiParamExample {json} 请求示例
 *{
 *  "pid":1,
    "rid":5,
 * }
 *
 * @apiSuccessExample  {json} 返回示例
 {
    "errno": 0,
    "errmsg": "",
    "data": {
        "affectedRows": 1
    }
}
*/
  async deleteAction() {
    const dataList = await this.model('role_privilege').where(this.post()).delete();
    return this.success(dataList);
  }
};
