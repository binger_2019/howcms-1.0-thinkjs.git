const Base = require('./base.js');
module.exports = class extends Base {
  __before() {
    super.__before();
  }
  /**
   * @api {post} /auth/login/ 用户登录
   * @apiGroup auth
   *
   * @apiParam {String} name 用户名(必填)
   * @apiParam {String} password 密码(必填)
   * @apiParamExample {json} 请求示例
   * {
   *  "name":"admin",
   *  "password": "e10adc3949ba59abbe56e057f20f883e"
   * }
   *
   * @apiSuccessExample  {json} 返回示例
    {
      "errno": 0,
      "errmsg": "",
      "data": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiYWRtaW4iLCJpYXQiOjE2MTY2MzEwOTAsImV4cCI6MTYxNjcxNzQ5MH0.Ahsy5Qwig7jcCpJyWk4Gu8BBXq5IKM3-toJvLsSz138"
    }
  */
  async loginAction() {
    const userInfo = await this.model('user').login(this.post());
    if (userInfo && userInfo.id) {
      // 将用户id 和登录时间进行jwt加密，时间戳用来确保单点登录
      const obj = {
        userId: userInfo.id,
        name: userInfo.name,
        timeSign: new Date().getTime()
      };
      const token = await this.updateAuth(obj);
      return this.success(token);
    } else {
      return this.fail('登录失败');
    }
  }

  /**
   * @api {post} /auth/logout/ 用户退出登录
   * @apiGroup auth
   *
   * @apiSuccessExample  {json} 返回示例
   {
      "errno": 0,
      "errmsg": "",
      "data": "退出登录成功"
   }
  */
  async logoutAction() {
    this.updateAuth(null);
    return this.success('退出登录成功');
  }
};
