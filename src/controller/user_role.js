
/**
 * @api {post} /user_role/post/ 增加用户_角色
 * @apiGroup user_role
 *
 * @apiParam {Int} uid 用户编号（必填）
 * @apiParam {Int} rid 角色编号（必填）
 * @apiParam {Int} create_by 创建人
 * @apiParam {String} create_date 创建时间
 * @apiParamExample {json} 请求示例
 * {
 *  "uid":1
    "rid":5
    "create_by":1
    "create_date":1591669444
 * }
 *
 * @apiSuccessExample  {json} 返回示例
 {
    "errno": 0,
    "errmsg": "",
    "data": {
        "id": 5
    }
 }
 */

const BaseRest = require('./rest.js');

module.exports = class extends BaseRest {
  __before() {

  }

  /**
 * @api {Get} /user_role/get/ 获取用户_角色列表
 * @apiGroup user_role
 *
 * @apiParam {Int} uid 用户编号（选填）
 * @apiParam {Int} rid 角色编号（选填）
 * @apiParam {Int} create_by 创建人（选填）
 * @apiParamExample {json} 请求示例
 * {
 *  "uid":1,
    "rid":5,
    "create_by":1
 * }
 *
 * @apiSuccessExample  {json} 返回示例
  {
    "errno": 0,
    "errmsg": "",
    "data": [
        {
            "uid": 1,
            "rid": 4,
            "create_by": 1,
            "create_date": "1591669444"
        },
        {
            "uid": 1,
            "rid": 5,
            "create_by": 1,
            "create_date": "1591669444"
        }
    ]
}
*/
  async getAction() {
    const dataList = await this.model('user_role').where(this.post()).select();
    return this.success(dataList);
  }

  /**
 * @api {delete} /user_role/delete/ 删除用户_角色
 * @apiGroup user_role
 *
 * @apiParam {Int} uid 用户编号（选填）
 * @apiParam {Int} rid 角色编号（选填）
 * @apiParam {Int} create_by 创建人（选填）
 * @apiParamExample {json} 请求示例
 *{
 *  "uid":1,
    "rid":5,
 * }
 *
 * @apiSuccessExample  {json} 返回示例
 {
    "errno": 0,
    "errmsg": "",
    "data": {
        "affectedRows": 1
    }
}
*/
  async deleteAction() {
    const dataList = await this.model('user_role').where(this.post()).delete();
    return this.success(dataList);
  }
};
