const Base = require('./rest.js');

module.exports = class extends Base {
  indexAction() {
    return this.display();
  }
  async postManyAction() {
    const data = this.post('data');
    const processDefinitionId = data[0].processDefinitionId;
    const currentTime = this.parseTime(new Date());
    // console.log(data);
    const data1 = [];
    const data2 = [];
    data.forEach((ele, index) => {
      ele.taskNode = index + 1;
      ele.candidate = ele.candidate.join(',');
      ele.update_time = currentTime;
      ele.create_time = currentTime;
      if (!ele.taskDefinitionId) {
        delete ele.taskDefinitionId;
        data2.push(ele);
      } else {
        data1.push(ele);
      }
    });
    // 需要先根据processDefinitionId删除信息
    const taskDefinitionModel = this.model('task_definition');
    try {
      await taskDefinitionModel.startTrans();
      await taskDefinitionModel.where({ processDefinitionId: processDefinitionId }).delete();
      if (data1.length > 0) {
        await taskDefinitionModel.addMany(data1);
      }
      if (data2.length > 0) {
        await taskDefinitionModel.addMany(data2);
      }
      // 流程定义表节点的数量
      const processDefinitionModel = this.model('process_definition').db(taskDefinitionModel.db());
      await processDefinitionModel.where({id: processDefinitionId}).update({processNodes: data.length});
      await taskDefinitionModel.commit();
      return this.success();
    } catch (e) {
      await taskDefinitionModel.rollback();
    }
  }
};
