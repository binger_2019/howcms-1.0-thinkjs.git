const Base = require('./base.js');
// const http = require('http');
// const url = require('url');
const superagent = require('superagent');
const cheerio = require('cheerio');
const async = require('async');
const eventproxy = require('eventproxy');
// const eq = new eventproxy();

module.exports = class extends Base {
  async indexAction() {
    var ep = new eventproxy();
    // 存放爬取网址
    const urlsArray = [];
    // 需要爬取文章的页数
    const pageNum = 2;

    for (var i = 0; i < pageNum; i++) {
      superagent
        .post('https://www.cnblogs.com/AggSite/AggSitePostList')
        .send({
          CategoryId: 808,
          CategoryType: 'SiteHome',
          ItemListActionName: 'AggSitePostList',
          PageIndex: i,
          ParentCategoryId: 0,
          TotalPostCount: 4000
        }).then(res => {
          const $ = cheerio.load(res.text);
          const curPageUrls = $('.post-item-title');
          for (var j = 0, len = curPageUrls.length; j < len; j++) {
            const articleUrl = curPageUrls[j].attribs.href;
            urlsArray.push(articleUrl);
            ep.emit('BlogArticleHtml', articleUrl);
          }
        });
    }
    ep.after('BlogArticleHtml', pageNum * 20, (articleUrls) => {
      // var curCount = 0;
      // function reptileMove(url, callBack) {
      //   var delay = parseInt((Math.random() * 30000000) % 1000, 10);
      //   curCount++;
      //   superagent
      //     .get(url)
      //     .then(res => {
      //       var $ = cheerio.load(res.text);
      //       console.log('现在的并发数是', curCount, '，正在抓取的是', url, '，耗时' + delay + '毫秒');
      //       // console.log($);
      //       setTimeout(function() {
      //         curCount--;
      //       }, delay * 10);
      //     });
      // }

      async.mapLimit(articleUrls, 2, async url => {
        await superagent.get(url).then(res => {
          return 12222;
          console.log(123, url);
        });
      // console.log(555, articleUrls);
      }, (err, results) => {
        if (err) throw err;
        // results is now an array of the response bodies
        console.log(1234, results);
      });
    });
  }
};
