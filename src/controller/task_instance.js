// const { success } = require('./base.js');
const Base = require('./rest.js');

module.exports = class extends Base {
  indexAction() {
    return this.display();
  }
  async myTaskAction() {
    const data = this.post();
    const { uid, taskStatus } = data;
    // 首先通过uid找到找到任务定义id
    const taskDefinitionIdArr = await this.model('task_definition').where('FIND_IN_SET(' + uid + ',candidate)').getField('taskDefinitionId');
    const where = { taskStatus: taskStatus === 0 ? taskStatus : ['!=', 0], assignee: uid };
    if (taskDefinitionIdArr.length > 0) {
      where.taskDefinitionId = ['IN', taskDefinitionIdArr];
    }
    // 找到对应的任务列表
    const taskInstanceList = await this.model('task_instance').page(this.post('page')).where(where).order('taskInstanceId DESC').countSelect();
    return this.success(taskInstanceList);
  }

  async updataTaskAction() {
    const data = this.post();
    // console.log(222, data);
    // 3、根据并签和会签更新流程
    // 4、生成下一个任务节点数据
    const { endTime, duration, taskStatus, comment, taskInstanceId, obj } = data;
    const taskInstanceModel = this.model('task_instance');
    const processInstanceModel = this.model('process_instance').db(taskInstanceModel.db());
    try {
      await taskInstanceModel.startTrans();
      // 1、更新我的任务
      await taskInstanceModel.where({ taskInstanceId: taskInstanceId }).update({ endTime, duration, taskStatus, comment });
      // 2、判断是会签还是并签
      // 会签,判断是不是所有人都同意，并审核通过
      if (obj.task_definition.taskType === 1) {
        const tastAssinFlag = await taskInstanceModel.where({ businessKey: obj.businessKey, taskNode: obj.taskNode, taskStatus: ['!=', 1] }).count();
        // 所有人都签了,并且都通过了，或者有人拒绝了
        if (!tastAssinFlag || taskStatus === 2) {
          this.updateProcessInstance(obj, taskInstanceModel, processInstanceModel, taskStatus);
        }
        // 如果有一个人拒绝，更新其他人的数据
        if (taskStatus === 2) {
          await taskInstanceModel.where({ businessKey: obj.businessKey, taskNode: obj.taskNode, taskInstanceId: ['!=', taskInstanceId], taskStatus: 0 }).update({ taskStatus: 10 });
        }
      } else {
        // 并签，只有一个人签就行
        // 更新其他人的数据
        await taskInstanceModel.where({ processInstanceId: obj.processInstanceId, taskNode: obj.taskNode, taskStatus: 0 }).update({ taskStatus: 10 });
        this.updateProcessInstance(obj, taskInstanceModel, processInstanceModel, taskStatus);
      }
      await taskInstanceModel.commit();
      return this.success();
    } catch (e) {
      await taskInstanceModel.rollback();
    }
  }
  async updateProcessInstance(obj, taskInstanceModel, processInstanceModel, processStatus) {
    // console.log(8868, obj);
    // 如果不是最后一个节点, 并且状态是通过;
    if (obj.taskNode < obj.process_definition.processNodes && processStatus === 1) {
      // 更新流程实例，主要是更新当前节点值
      await processInstanceModel.where({ processInstanceId: obj.process_instance.processInstanceId }).update({ currentProcessNode: obj.taskNode });
      // 生成下一个节点的任务实例
      const nextTaskNode = obj.taskNode + 1;
      const nextTask = await this.model('task_definition').where({ processDefinitionId: obj.processDefinitionId, taskNode: nextTaskNode }).find();
      const taskInstanceFormArr = [];
      nextTask.candidate.split(',').forEach(ele => {
        taskInstanceFormArr.push({
          processInstanceId: obj.processInstanceId,
          processDefinitionId: obj.processDefinitionId,
          businessKey: obj.businessKey,
          taskDefinitionId: nextTask.taskDefinitionId,
          taskName: nextTask.taskName,
          taskNode: nextTask.taskNode,
          startTime: this.parseTime(new Date()),
          taskStatus: 0,
          assignee: ele,
          tableName: obj.process_instance.tableName
        });
      });
      await taskInstanceModel.addMany(taskInstanceFormArr);
    } else {
      // 更新流程实例，结束该实例
      const updateForm = {
        endTime: this.parseTime(new Date()),
        duration: new Date().getTime() - new Date(obj.process_instance.startTime).getTime(),
        processStatus: processStatus,
        currentProcessNode: obj.taskNode
      };
      await processInstanceModel.where({ processInstanceId: obj.process_instance.processInstanceId }).update(updateForm);
    }
  }
  // 获取流程表单数据
  async getTableDataAction() {
    const { businessKey, tableName } = this.post();
    const sql = "SELECT COLUMN_NAME as field_name , COLUMN_COMMENT as remark  FROM information_schema.COLUMNS WHERE TABLE_NAME = '" + tableName + "'";
    const model = this.model(tableName);
    const remark = await model.query(sql);
    const res = await model.where({ businessKey }).find();
    const arr = [];
    remark.forEach(ele => {
      ele.value = res[ele.field_name];
      if (ele.remark) {
        arr.push(ele);
      }
    });
    return this.success(arr);
  }
};
