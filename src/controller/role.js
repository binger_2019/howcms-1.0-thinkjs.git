/**
 * @api {Get} /role/get/ 获取角色列表
 * @apiGroup role
 *
 * @apiParam {Int} id 角色id(选填)
 * @apiParamExample {json} 请求示例
 * {
 *  id:1
 * }
 *
 * @apiSuccessExample  {json} 返回示例
  {
    "errno": 0,
    "errmsg": "",
    "data": [
        {
            "id": 1,
            "name": "admin"
        }
    ]
  }
*/

const BaseRest = require('./rest.js');

module.exports = class extends BaseRest {
  __before() {

  }
  // 重写保存方法
  /**
       * @api {post} /role/post/ 增加角色
       * @apiGroup role
       *
       * @apiParam {String} name 角色名（必填）
       * @apiParam {Array} menuIds 角色-权限
       * @apiParam {String} remark 角色备注
       * @apiParamExample {json} 请求示例
       * {
       *  menuIds: [38, 40, 27],
          name: "4台人员",
          remark: "微软天后宫"
       * }
       *
       * @apiSuccessExample  {json} 返回示例
       {
          "errno": 0,
          "errmsg": "",
          "data": {
              "id": 5
          }
       }
       */
  async postAction() {
    const data = this.post();
    const roleModel = this.model('role');
    const uid = this.ctx.header.uid;
    try {
      await roleModel.startTrans();
      const arr = [];
      const rid = await roleModel.add(data);
      data.menuIds.forEach(ele => {
        arr.push({
          rid: rid,
          pid: ele,
          create_by: uid
        });
      });
      const rolePrivilegeModel = this.model('role_privilege').db(roleModel.db());
      await rolePrivilegeModel.addMany(arr);
      await roleModel.commit();
      return this.success({ rid: rid });
    } catch (e) {
      await roleModel.rollback();
    }
  }
  /**
     * @api {put} /role/put/ 编辑角色
     * @apiGroup role
     *
     * @apiParam {Int} id 角色Id（必填）
     * @apiParam {String} name 角色名（必填）
     * @apiParam {Array} menuIds 角色-权限
     * @apiParam {String} remark 角色备注
     * @apiParamExample {json} 请求示例
     * {
          id: 26
          menuIds: [33, 27]
          name: "test"
          remark: "备注"
     * }
     *
     * @apiSuccessExample  {json} 返回示例
    {
        "errno": 0,
        "errmsg": "",
        "data":''
    }
     */
  async putAction() {
    const data = this.post();
    const roleModel = this.model('role');
    const uid = this.ctx.header.uid;
    try {
      await roleModel.startTrans();
      const arr = [];
      await roleModel.update(this.post());
      data.menuIds.forEach(ele => {
        arr.push({
          rid: this.post('id'),
          pid: ele,
          create_by: uid
        });
      });
      const rolePrivilegeModel = this.model('role_privilege').db(roleModel.db());
      await rolePrivilegeModel.where({ rid: this.post('id') }).delete();
      await rolePrivilegeModel.addMany(arr);
      await roleModel.commit();
      return this.success();
    } catch (e) {
      await roleModel.rollback();
    }
  }

  /**
       * @api {delete} /role/delete/ 删除角色
       * @apiGroup role
       *
       * @apiParam {Int} id 角色id（必填）
       * @apiParamExample {json} 请求示例
       * {
       *  "id": "5",
       * }
       *
       * @apiSuccessExample  {json} 返回示例
       {
          "errno": 0,
          "errmsg": "",
          "data": {
              "affectedRows": 1
          }
      }
      */
  async deleteAction() {
    const roleModel = this.model('role');
    try {
      await roleModel.startTrans();
      // 删除角色
      await roleModel.where({ id: this.post('id') }).delete();
      // 删除角色权限
      const rolePrivilegeModel = this.model('role_privilege').db(roleModel.db());
      await rolePrivilegeModel.where({ rid: this.post('id') }).delete();
      await roleModel.commit();
      return this.success();
    } catch (e) {
      await roleModel.rollback();
    }
  }
};
