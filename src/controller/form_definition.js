const Base = require('./rest.js');

module.exports = class extends Base {
  indexAction() {
    return this.display();
  }
  async addAction() {
    const {name, rule, options, id} = this.post();
    const createTime = this.parseTime(new Date());
    // 定义自定义表单数据
    const formDefinitionObj = {
      name,
      options: JSON.stringify(options),
      create_time: createTime
    };
    if (id) {
      formDefinitionObj.id = id;
    }
    const formDefinitionModel = this.model('form_definition');
    try {
      await formDefinitionModel.startTrans();
      const formDefinitionId = await formDefinitionModel.add(formDefinitionObj, {
        replace: true
      });
      const formDefinitionFieldsModel = this.model('form_definition_fields').db(formDefinitionModel.db());
      // 如果是更新的话，先删掉实例数据
      if (id) {
        formDefinitionFieldsModel.where({form_definition_id: id}).delete();
      }
      rule.forEach(async(ele, index) => {
        ele.sort = index + 1;
        ele.form_definition_id = formDefinitionId;
        if (id) {
          ele.update_time = createTime;
        } else {
          ele.create_time = createTime;
        }
        for (var i in ele) {
          if (ele[i] && ['boolean', 'object'].includes(typeof ele[i])) {
            ele[i] = JSON.stringify(ele[i]);
          }
        }
        // 只能逐条增加
        await formDefinitionFieldsModel.add(ele, {
          replace: true
        });
      });
      await formDefinitionModel.commit();
      return this.success(formDefinitionId);
    } catch (e) {
      await formDefinitionModel.rollback();
    }
  }
};
