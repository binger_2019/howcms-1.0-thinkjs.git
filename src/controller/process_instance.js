const Base = require('./rest.js');

module.exports = class extends Base {
  indexAction() {
    return this.display();
  }
  async postAction() {
    // console.log(this.post());
    const data = this.post();
    const currentTime = this.parseTime(new Date());
    data.create_time = currentTime;
    data.update_time = currentTime;
    // const tableName = data.tableName;
    const { tableName, processDefinitionId, businessKey, startUserId } = data;
    const processInstanceModel = this.model('process_instance');
    try {
      await processInstanceModel.startTrans();
      // 这个是新增数据的表单，数据表名称需要传进来
      const tableModel = this.model(tableName).db(processInstanceModel.db());
      // 首先把表单数据加进去
      await tableModel.add(data);
      // 在流程实例表里面添加数据，并拿到该条数据的id
      const processInstanceForm = {
        processDefinitionId: processDefinitionId,
        businessKey: businessKey,
        startTime: currentTime,
        startUserId: startUserId,
        currentProcessNode: 0,
        processStatus: 0,
        deleted: 0,
        tableName: tableName
      };
      const processInstanceId = await processInstanceModel.add(processInstanceForm);
      // 需要找到流程定义的第一个任务
      const taskDefinitionModel = this.model('task_definition').db(processInstanceModel.db());
      const taskDefinition = await taskDefinitionModel.where({ processDefinitionId: processDefinitionId, taskNode: 1 }).find();
      // console.log(taskDefinition);
      const {taskDefinitionId, taskName} = taskDefinition;
      // 向任务实例表里面添加数据
      const taskInstanceModel = this.model('task_instance').db(processInstanceModel.db());
      const taskInstanceFormArr = [];
      taskDefinition.candidate.split(',').forEach(ele => {
        taskInstanceFormArr.push({
          processInstanceId: processInstanceId,
          processDefinitionId: processDefinitionId,
          businessKey: businessKey,
          taskDefinitionId: taskDefinitionId,
          taskName: taskName,
          taskNode: 1,
          startTime: currentTime,
          taskStatus: 0,
          assignee: ele,
          tableName: tableName
        });
      });
      await taskInstanceModel.addMany(taskInstanceFormArr);
      await processInstanceModel.commit();
      return this.success();
    } catch (e) {
      await processInstanceModel.rollback();
    }
  }
  async deleteAction() {
    // 删除流程实例的话，要同时删除task_instance和数据表中的数据
    try{
      const {businessKey, tableName} = this.post();
      await this.model(tableName).setRelation(false).where({businessKey}).delete();
      await this.model('task_instance').setRelation(false).where({businessKey}).delete();
      await this.model('process_instance').setRelation(false).where({businessKey}).delete();
      return this.success();
    }catch{
      return this.success();
    }

  }
};
