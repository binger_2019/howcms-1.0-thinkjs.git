const assert = require('assert');
const Base = require('./base.js');
module.exports = class extends Base {
  static get _REST() {
    return true;
  }

  constructor(ctx) {
    super(ctx);
    this.resource = this.getResource();
    this.id = this.getId();
    assert(think.isFunction(this.model), 'this.model must be a function');
    this.modelInstance = this.model(this.resource);
  }
  __before() {
    // super.__before();
  }

  /**
   * get resource
   * @return {String} [resource name]
   */
  getResource() {
    return this.ctx.controller.split('/').pop();
  }
  getId() {
    const id = this.get('id') || this.post('id');
    if (id && (think.isString(id) || think.isNumber(id))) {
      return id;
    }
    // const last = this.ctx.path.split('/').slice(-1)[0];
    // if (last !== this.resource) {
    //   return last;
    // }
    return '';
  }
  async getAction() {
    let data;
    const pk = this.modelInstance.pk;
    if (this.id) {
      data = await this.modelInstance.where({ [pk]: this.id }).find();
      return this.success(data);
    }
    // 如果有分页参数
    let order = {};
    if (this.post('order') && this.post('order') instanceof Object) {
      order = Object.assign(order, this.post('order'));
    }
    if (order[pk]) {
      order[pk] = 'DESC';
    }

    // 通过setRelation部分关闭关联关系
    const setRelation = this.post('setRelation');
    if (this.post('page')) {
      if (setRelation) {
        data = await this.modelInstance.setRelation(setRelation, false).page(this.post('page')).where(this.post('where')).order(order).countSelect();
      } else {
        data = await this.modelInstance.page(this.post('page')).where(this.post('where')).order(order).countSelect();
      }
    } else {
      if (setRelation) {
        data = await this.modelInstance.setRelation(setRelation, false).where(this.post('where')).order(order).select();
      } else {
        data = await this.modelInstance.where(this.post('where')).order(order).select();
      }
    }
    return this.success(data);
  }
  /**
   * put resource
   * @return {Promise} []
   */
  async postAction() {
    const pk = this.modelInstance.pk;
    const data = this.post();
    delete data[pk];
    data['create_time'] = this.parseTime(new Date());
    if (think.isEmpty(data)) {
      return this.fail('数据为空');
    }
    try {
      const insertId = await this.modelInstance.add(data);
      return this.success({ id: insertId });
    } catch (err) {
      return this.fail('新增数据失败，检查是否有重复约束');
    }
  }
  /**
   * delete resource
   * @return {Promise} []
   */
  async deleteAction() {
    // console.log(123, this.post());
    if (this.id) {
      const pk = this.modelInstance.pk;
      const rows = await this.modelInstance.where({ [pk]: this.id }).delete();
      return this.success({ affectedRows: rows });
    } else {
      const rows = await this.modelInstance.where(this.post()).delete();
      return this.success({ affectedRows: rows });
      // return this.fail('params error');
    }
  }
  /**
   * update resource
   * @return {Promise} []
   */
  async putAction() {
    if (!this.id) {
      return this.fail('params error');
    }
    const pk = this.modelInstance.pk;
    const data = this.post();
    data[pk] = this.id; // rewrite data[pk] forbidden data[pk] !== this.id
    data['update_time'] = this.parseTime(new Date());
    if (think.isEmpty(data)) {
      return this.fail('数据为空');
    }
    try {
      const rows = await this.modelInstance.where({ [pk]: this.id }).update(data);
      return this.success({ affectedRows: rows });
    } catch (err) {
      return this.fail('更改数据失败，检查是否有重复约束');
    }
  }
  __call() { }
};
