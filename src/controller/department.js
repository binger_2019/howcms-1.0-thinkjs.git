/**
 * @api {Get} /department/get/ 获取单位列表
 * @apiGroup department
 *
 * @apiParam {Int} id 单位id(选填)
 * @apiParamExample {json} 请求示例
 * {
 *  id:1
 * }
 *
 * @apiSuccessExample  {json} 返回示例
{
    "errno": 0,
    "errmsg": "",
    "data": {
        "id": 72,
        "name": "蓝天小区",
        "pid": 0,
        "mpSetting": null,
        "dashboardSetting": null,
        "orderNum": null,
        "create_date": "1591669444"
    }
}
*/

/**
 * @api {post} /department/post/ 增加单位
 * @apiGroup department
 *
 * @apiParam {String} name 单位名（必填）
 * @apiParam {Int} pid 父id（必填）
 * @apiParam {String} mp_setting 小程序首页设置参数(json格式)
 * @apiParam {String} dashboard_setting 首页大屏幕设置参数(json格式)
 * @apiParam {Int} orderNum 排序（越大越靠前）
 * @apiParamExample {json} 请求示例
 * {
        "name": "蓝天小区",
        "pid": 0,
        "mpSetting": null,
        "dashboardSetting": null,
        "orderNum": null,
        "create_date": "1591669444"
 * }
 *
 * @apiSuccessExample  {json} 返回示例
{
    "errno": 0,
    "errmsg": "",
    "data": {
        "id": 105
    }
}
 */

/**
 * @api {put} /department/put/ 编辑单位
 * @apiGroup department
 *
 * @apiParam {Int} id 单位Id（必填）
 * @apiParam {String} name 单位名
 * @apiParam {Int} pid 父id
 * @apiParam {String} mp_setting 小程序首页设置参数(json格式)
 * @apiParam {String} dashboard_setting 首页大屏幕设置参数(json格式)
 * @apiParam {Int} orderNum 排序（越大越靠前）
 * @apiParamExample {json} 请求示例
 *  {
        "name": "蓝天小区",
        "pid": 0,
        "mpSetting": null,
        "dashboardSetting": null,
        "orderNum": null,
        "create_date": "1591669444",
        "id":1
  }
 *
 * @apiSuccessExample  {json} 返回示例
{
    "errno": 0,
    "errmsg": "",
    "data": {
        "affectedRows": 1
    }
}
 */

/**
 * @api {delete} /department/delete/ 删除单位
 * @apiGroup department
 *
 * @apiParam {Int} id 单位id（必填）
 * @apiParamExample {json} 请求示例
 * {
 *  "id": "5",
 * }
 *
 * @apiSuccessExample  {json} 返回示例
 {
    "errno": 0,
    "errmsg": "",
    "data": {
        "affectedRows": 1
    }
}
*/

const BaseRest = require('./rest.js');

module.exports = class extends BaseRest {
  __before() {

  }

  /**
       * @api {post} /department/getTree/ 生成单位树状列表
       * @apiGroup department
       *
       * @apiParam {Int} pid 单位根id（必填）
       * @apiParamExample {json} 请求示例
       * {
       *  "pid": "94",
       * }
       *
       * @apiSuccessExample  {json} 返回示例
       {
      "errno": 0,
      "errmsg": "",
      "data": [
                  {
                      "id": 99,
                      "name": "一班",
                      "pid": 94,
                      "mp_setting": null,
                      "dashboard_setting": null,
                      "orderNum": 0,
                      "create_date": "1596162418",
                      "path": "一班"
                  },
                  {
                      "id": 100,
                      "name": "二班",
                      "pid": 94,
                      "mp_setting": null,
                      "dashboard_setting": null,
                      "orderNum": 0,
                      "create_date": "1596162423",
                      "path": "二班"
                  }
              ]
          }
      */
  async getTreeAction() {
    const where = {};
    const param = this.post();
    if (param.did) {
      where.id = ['IN', param.did];
    }
    const dataList = await this.model('department').where(where).order('orderNum DESC').select();
    const res = this.recursionDataTree(dataList, param.pid);
    // 如果不是父子结构，则递归不到，直接返回
    return this.success(res || dataList);
  }

  /**
                * @api {post} /department/getPath/ 生成具有路径的单位列表
                * @apiGroup department
                *
                * @apiParam {Int} pid 单位根id（必填）
                * @apiParamExample {json} 请求示例
                * {
                *  "pid": "94",
                * }
                *
                * @apiSuccessExample  {json} 返回示例
                {
            "errno": 0,
            "errmsg": "",
            "data": [
                {
                    "id": 94,
                    "name": "二连",
                    "pid": 91,
                    "mp_setting": null,
                    "dashboard_setting": null,
                    "orderNum": 0,
                    "create_date": "1596162380",
                    "path": "二连"
                },
                {
                    "id": 99,
                    "name": "一班",
                    "pid": 94,
                    "mp_setting": null,
                    "dashboard_setting": null,
                    "orderNum": 0,
                    "create_date": "1596162418",
                    "path": "二连/一班"
                },
                {
                    "id": 100,
                    "name": "二班",
                    "pid": 94,
                    "mp_setting": null,
                    "dashboard_setting": null,
                    "orderNum": 0,
                    "create_date": "1596162423",
                    "path": "二连/二班"
                }
            ]
        }
        */

  async getPathAction() {
    const dataList = await this.model('department').order('orderNum DESC').select();
    const param = this.post();
    const res = await this.recursionDataPath(dataList, param.pid);
    return this.success(res);
  }

  /**
                * @api {post} /department/getAuth/ 获取本级及所有子级节点
                * @apiGroup department
                *
                * @apiParam {Int} pid 单位根id（必填）
                * @apiParamExample {json} 请求示例
                * {
                *  "pid": "73",
                * }
                *
                * @apiSuccessExample  {json} 返回示例
               {
                  "errno": 0,
                  "errmsg": "",
                  "data": [
                      {
                          "id": 91
                      },
                      {
                          "id": 92
                      },
                      {
                          "id": 101
                      }
                  ]
              }
        */

  async getAuthAction() {
    const query = `
        SELECT
    	  id
        FROM
    	(
        SELECT
    	   t1.id,
         IF
    	  (find_in_set( pid, @pids ) > 0, @pids := concat( @pids, ',', id ), 0 ) AS ischild,
    	  t1.pid
         FROM
      	   ( SELECT id, pid FROM department t  ORDER BY pid, id ) t1,
    	   ( SELECT @pids := ` + this.post('pid') + ` ) t2
    	   ) t3
        WHERE
    	   ischild != 0`;
    const res = await this.model('department').query(query);
    return this.success(res);
  }
};
