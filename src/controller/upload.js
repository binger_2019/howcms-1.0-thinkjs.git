const BaseRest = require('./rest.js');
module.exports = class extends BaseRest {
  async indexAction() {
    const fs = require('fs');
    const themefile = this.file('file');
    const filepath = themefile.path;// 为防止上传的时候因文件名重复而覆盖同名已上传文件，path是MD5方式产生的随机名称
    const uploadpath = think.ROOT_PATH + '/www/uploads';
    think.mkdir(uploadpath);// 创建该目录
    // 获取文件后缀名，为了使保存的文件唯一
    // 获取最后一个.的位置
    var index = themefile.name.lastIndexOf('.');
    // 获取后缀
    var ext = themefile.name.substr(index);
    const basename = think.uuid() + ext;// 因为本系统不允许上传同名主题，所以文件名就直接使用主题名
    var readStream = fs.createReadStream(filepath);
    var writeStream = fs.createWriteStream('./www/uploads/' + basename);
    readStream.pipe(writeStream);
    readStream.on('end', function() {
      fs.unlinkSync(filepath);
    });
    themefile.path = 'uploads/' + basename;
    this.success(themefile);
  }
};
