/**
 * @api {Get} /user/get/ 获取用户列表
 * @apiGroup User
 *
 * @apiParam {Int} id 用户id(选填)
 * @apiParamExample {json} 请求示例
 * {
 *  id:1
 * }
 *
 * @apiSuccessExample  {json} 返回示例
  {
    "errno": 0,
    "errmsg": "",
    "data": [
        {
            "id": 1,
            "code": null,
            "name": "admin",
            "password": "e10adc3949ba59abbe56e057f20f883e"
        }
    ]
  }
*/
const BaseRest = require('./rest.js');

module.exports = class extends BaseRest {
  __before() {

  }
  // 重写保存方法
  /**
       * @api {post} /role/post/ 增加用户
       * @apiGroup role
       *
       * @apiParam {String} name 用户名（必填）
       * @apiParam {Array} menuIds 用户-权限
       * @apiParam {String} remark 用户备注
       * @apiParamExample {json} 请求示例
       * {
       *  menuIds: [38, 40, 27],
          name: "4台人员",
          remark: "微软天后宫"
       * }
       *
       * @apiSuccessExample  {json} 返回示例
       {
          "errno": 0,
          "errmsg": "",
          "data": {
              "id": 5
          }
       }
       */
  async postAction() {
    const data = this.post();
    const userModel = this.model('user');
    try {
      await userModel.startTrans();
      const arr = [];
      const uid = await userModel.add(data);
      data.rids.forEach(ele => {
        arr.push({
          uid: uid,
          rid: ele,
          create_by: this.ctx.header.uid
        });
      });
      const userRoleModel = this.model('user_role').db(userModel.db());
      await userRoleModel.addMany(arr);
      await userModel.commit();
      return this.success('保存成功');
    } catch (e) {
      await userModel.rollback();
    }
  }

  /**
 * @api {delete} /user/delete/ 删除用户
 * @apiGroup User
 *
 * @apiParam {Int} id 用户id（必填）
 * @apiParamExample {json} 请求示例
 * {
 *  "id": "5",
 * }
 *
 * @apiSuccessExample  {json} 返回示例
 {
    "errno": 0,
    "errmsg": "",
    "data": {
        "affectedRows": 1
    }
}
*/
  async deleteAction() {
    const userModel = this.model('user');
    try {
      await userModel.startTrans();
      await userModel.delete(this.post('id'));
      const userRoleModel = this.model('user_role').db(userModel.db());
      await userRoleModel.where({ uid: this.post('id') }).delete();
      await userModel.commit();
      return this.success();
    } catch (e) {
      await userModel.rollback();
    }
  }

  /**
         * @api {put} /role/put/ 编辑用户
         * @apiGroup role
         *
         * @apiParam {Int} id 用户Id（必填）
         * @apiParam {String} name 用户名（必填）
         * @apiParam {Array} menuIds 用户-权限
         * @apiParam {String} remark 用户备注
         * @apiParamExample {json} 请求示例
         * {
              id: 26
              menuIds: [33, 27]
              name: "test"
              remark: "备注"
         * }
         *
         * @apiSuccessExample  {json} 返回示例
        {
            "errno": 0,
            "errmsg": "",
            "data":''
        }
         */

  async putAction() {
    const data = this.post();
    const userModel = this.model('user');
    try {
      await userModel.startTrans();
      const arr = [];
      await userModel.update(this.post());
      data.rids.forEach(ele => {
        arr.push({
          uid: this.post('id'),
          rid: ele,
          create_by: this.ctx.header.uid
        });
      });
      const userRoleModel = this.model('user_role').db(userModel.db());
      await userRoleModel.where({ uid: this.post('id') }).delete();
      await userRoleModel.addMany(arr);
      await userModel.commit();
      return this.success();
    } catch (e) {
      await userModel.rollback();
    }
  }
  /**
 * @api {get} /user/user/ 获取登录用户的id
 * @apiGroup User
 *

 * @apiSuccessExample  {json} 返回示例
{
    "errno": 0,
    "errmsg": "",
    "data": {
        "id": 1,
        "iat": 1616983497,
        "exp": 1617069897
    }
}
*/
  async userAction() {
    const userInfo = this.ctx.state.userId;
    if (userInfo) {
      return this.success(userInfo);
    } else {
      return this.fail('获取用户信息失败');
    }
  }

  /**
 * @api {get} /user/checkPass/ 验证原始密码是否正确
 * @apiGroup User
 *
  * @apiParam {Int} id 用户Id（必填）
  * @apiParam {String} password 密码（必填）
 * @apiSuccessExample  {json} 返回示例
{
    "errno": 0,
    "errmsg": "",
    "data": 1
}
*/
  async checkPassAction() {
    const res = await this.model('user').where(this.post()).count();
    return this.success(res);
  }

  /**
 * @api {get} /user/checkPass/ 验证原始密码是否正确
 * @apiGroup User
 *
  * @apiParam {Int} id 用户Id（必填）
  * @apiParam {String} password 密码（必填）
 * @apiSuccessExample  {json} 返回示例
{
    "errno": 0,
    "errmsg": "",
    "data": 1
}
*/
  async editPassAction() {
    const res = await this.model('user').where({id: this.post('id')}).update({password: this.post('password')});
    return this.success(res);
  }
};
