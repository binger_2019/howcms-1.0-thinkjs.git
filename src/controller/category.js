const BaseRest = require('./rest.js');
module.exports = class extends BaseRest {
  /**
     * @api {post} /privilege/getTree/ 生成分类树状列表
     * @apiGroup privilege
     *
     * @apiParam {Int} pid 分类根id（必填）
     * @apiParamExample {json} 请求示例
     * {
     *  "pid": "94"
     * }
     *
     * @apiSuccessExample  {json} 返回示例
     {
    "errno": 0,
    "errmsg": "",
    "data": [

    ]
}
    */
  async getTreeAction() {
    const where = {};
    const param = this.post();
    const dataList = await this.model('category').where(where).order('orderNum DESC').select();
    const res = this.recursionDataTree(dataList, param.pid);
    return this.success(res);
  }
};
