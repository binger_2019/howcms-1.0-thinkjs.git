// default config
module.exports = {
  workers: 1,
  cookie: {
    domain: '',
    path: '/',
    maxAge: 10 * 3600 * 1000, // 10个小时
    signed: true,
    keys: ['389105'] // 当 signed 为 true 时，使用 keygrip 库加密时的密钥
  },
  jwt: {
    secret: '389105',
    cookie: 'jwt-token',
    expire: 60 * 60 * 24 // 秒
  }
};
