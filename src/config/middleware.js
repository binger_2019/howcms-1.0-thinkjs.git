const path = require('path');
// const jwt = require('koa-jwt');
const isDev = think.env === 'development';
// const cors = require('koa2-cors');
module.exports = [
  {
    handle: 'meta',
    options: {
      logRequest: isDev,
      sendResponseTime: isDev
    }
  },
  // {
  //   handle: cors, // 全局处理跨域，所有请求都会允许跨域，如果想要进行相关配置请看kcors文档进行配置
  //   options: {
  //     origin: (ctx) => { // 设置允许来自指定域名请求
  //       return isDev ? 'http://localhost:9528' : 'http://thinkjs.howeaver.cn'; // 只允许http://localhost:8080这个域名的请求
  //     },
  //     credentials: true, // 是否允许发送Cookie
  //     allowMethods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'], // 设置所允许的HTTP请求方法
  //     allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'jwt-token'], // 设置服务器支持的所有头信息字段
  //     exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'] // 设置获取其他自定义字段
  //   }
  // },
  {
    handle: 'resource',
    // enable: isDev,
    options: {
      root: path.join(think.ROOT_PATH, 'www'),
      publicPath: /^\/(static|favicon\.ico|uploads|apidoc)/
    }
  },
  {
    handle: 'trace',
    enable: !think.isCli,
    options: {
      debug: isDev
    }
  },
  {
    handle: 'payload',
    options: {
      keepExtensions: true,
      limit: '5mb'
    }
  },
  {
    handle: 'router',
    options: {}
  },

  // {
  //   handle: jwt,
  //   options: {
  //     cookie: think.config('jwt')['cookie'],
  //     secret: think.config('jwt')['secret'],
  //     passthrough: true
  //   }
  // },
  {
    handle: 'recordLog'
  },
  'logic',
  'controller'

];
