const jsonwebtoken = require('jsonwebtoken');
module.exports = options => {
  return async(ctx, next) => {
    const token = ctx.header['jwt-token'];
    const { secret } = ctx.config('jwt');
    var tokenObj = token ? jsonwebtoken.verify(token, secret) : {};
    const obj = {
      responseURL: ctx.url,
      requestData: ctx.post(),
      uname: tokenObj.name,
      uid: tokenObj.userId,
      create_time: think.datetime(new Date()),
      method: ctx.method,
      ip: ctx.ip
    };
    const apiData = require(think.ROOT_PATH + '/www/apidoc/api_data.json');
    const targetObj = apiData.filter((item) => {
      return item.url === obj.responseURL;
    })[0];
    if (targetObj) {
      const form = Object.assign(obj, targetObj);
      try {
        await ctx.model('log').add(form);
        return next();
      }
      catch{
        return next();
      }
    }else{
      return next();
    }
  };
};
